<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>File Sharer</title>
	<link rel="stylesheet" href="style.css" />
	<script type="application/javascript" src="jquery-3.3.1.min.js"></script>
	<script type="application/javascript" src="app.js"></script>
</head>
<body>
	<?php
        session_start();
		//delete file
		if(isset($_SESSION["ok"]) && isset($_POST["delete"])){
			$fileName = $_POST["delete"];
			$toDelete ="static/".$fileName;
			if(file_exists($toDelete)){
				unlink($toDelete);
				echo("Plik $fileName został usunięty.");
			}
			else{
				echo("Plik nie istnieje! <br/>");
			}
		}
		if(isset($_POST["submit"])){
			$targetDir = "static/";
			$targetFile = $targetDir.basename($_FILES["fileToUpload"]["name"]);
			if(file_exists($targetFile)){
				echo("Plik już istnieje! <br/>");
			}
			else{
				//add file
				if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $targetFile)) {
					echo "Plik ". basename( $_FILES["fileToUpload"]["name"]). " został wrzucony.";
				} else {
					echo "Sorry, ale coś nie pykło.";
				}
			}
		}
        if(isset($_POST["log"])){
            if($_POST["password"] == "hasloTajne"){
                $_SESSION["ok"] = 1;
            }
        }
	?>
	
	
	
	<?php
    if(isset($_SESSION["ok"])){
	    $dir = 'static/';
	    $files = scandir($dir);
	    for($i = 0; $i<count($files); $i++){
		    if($files[$i][0] != "."){
		    		echo("<div class='fileDiv'><a href='static/".$files[$i]."' target='_blank'>".$files[$i]."</a>");
				echo("<form method='post' class='delButton' id='".$files[$i]."' onsubmit='return deleteClick(this)'><input type='hidden' name='delete' value='".$files[$i]."'/><input type='submit' value='Usuń'/></form>");
				echo("</div>");
			}
        }
    }
    else{
        echo("<form method='post'><input name='password' type='password' autofocus /><input type='submit' name='log' value='log' /></form>");
    }
	?>
	<div id="overlay"></div>
	<div id="loadWrapper">
		<img id="load" src="load.png" width="100" height="100" />
	</div>
	<br />
	<form action="index.php" method="post" enctype="multipart/form-data" onsubmit="startLoad()">
		Select file to upload:
		<input type="file" name="fileToUpload" id="fileToUpload">
		<input type="submit" value="Upload File" name="submit">
	</form>
</body>

</html>
